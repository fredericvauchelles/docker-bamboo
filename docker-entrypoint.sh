#!/bin/bash
set -e

mkdir -p "$CONF_HOME";
chmod 700 "$CONF_HOME";
chown -R bamboo: "$CONF_HOME";

exec gosu bamboo "${CONF_INSTALL}/bin/start-bamboo.sh" "-fg";

exec "$@"
